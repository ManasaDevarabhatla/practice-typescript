var getvalue = function(){
    return 10;
}
console.log(getvalue());

//three lines of code above can be written in a single statement
const getvalue1 = () => 10;
console.log(getvalue1());

const getvalue2 = (m,bonus) => 10*m+bonus;
console.log(getvalue2(10,10));