//destructing arrays = pulling aprt the elements of an array

let employee = ["monica","geller","female","30"];
let [fname,lname,gender] = employee;
let [, , ,ag] = employee;
console.log(fname);
console.log(lname);
console.log(gender);
console.log(ag);

//destructuring objects

let employee2={
    fname2: "Rachel",
    lname2: "Green",
    gender2: "Female"
};
let{fname2: f,lname2,gender2} = employee2;

console.log(f);
console.log(lname2);
console.log(gender2);
