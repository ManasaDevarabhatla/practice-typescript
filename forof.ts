let colorss =['red','blue','green'];

for(let index in colorss){
    console.log(colorss[index]);
}

// use of "for of"
for(let col of colorss){
    console.log(col);
}

let letters = "ABSC";
for(let letter of letters){
    console.log(letter);
}