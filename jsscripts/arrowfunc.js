var getvalue = function () {
    return 10;
};
console.log(getvalue());
//three lines of code above can be written in a single statement
var getvalue1 = function () { return 10; };
console.log(getvalue1());
var getvalue2 = function (m, bonus) { return 10 * m + bonus; };
console.log(getvalue2(10, 10));
//# sourceMappingURL=arrowfunc.js.map