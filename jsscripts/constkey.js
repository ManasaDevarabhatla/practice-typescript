// const should be initialised at the point of declaration
var num = 10;
var obj = {
    name: "Manasa"
};
console.log(obj.name);
//const objects can only be assigned a new value to the property of the object
obj.name = "Krishna";
console.log(obj.name);
//let vs const
//use let when reassignment is expected or otherwise
//# sourceMappingURL=constkey.js.map