//destructing arrays = pulling aprt the elements of an array
var employee = ["monica", "geller", "female", "30"];
var fname = employee[0], lname = employee[1], gender = employee[2];
var ag = employee[3];
console.log(fname);
console.log(lname);
console.log(gender);
console.log(ag);
//destructuring objects
var employee2 = {
    fname2: "Rachel",
    lname2: "Green",
    gender2: "Female"
};
var f = employee2.fname2, lname2 = employee2.lname2, gender2 = employee2.gender2;
console.log(f);
console.log(lname2);
console.log(gender2);
//# sourceMappingURL=destruct.js.map