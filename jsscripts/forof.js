var colorss = ['red', 'blue', 'green'];
for (var index in colorss) {
    console.log(colorss[index]);
}
// use of "for of"
for (var _i = 0, colorss_1 = colorss; _i < colorss_1.length; _i++) {
    var col = colorss_1[_i];
    console.log(col);
}
var letters = "ABSC";
for (var _a = 0, letters_1 = letters; _a < letters_1.length; _a++) {
    var letter = letters_1[_a];
    console.log(letter);
}
//# sourceMappingURL=forof.js.map