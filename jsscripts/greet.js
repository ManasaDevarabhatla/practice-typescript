//var functionality scope and hoisting
function greet(name) {
    if (name == "manasa") {
        var g = "hello Manasa";
    }
    else {
        var g = "hello there!";
    }
    console.log(g);
}
greet("manas");
//# sourceMappingURL=greet.js.map