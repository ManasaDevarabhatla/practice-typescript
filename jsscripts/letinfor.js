for (var i = 0; i <= 5; i++) {
    console.log(i);
}
var _loop_1 = function (i_1) {
    setTimeout(function () { console.log(i_1); }, 1000);
};
/** when we give var keyword it is refering to the end value of i but not the
 * value at each iteration
 *
 * for(var i=0;i<=5;i++){
    setTimeout(function(){console.log(i);},1000);
}*/
// therefore while using closures and loops let keyword is used.
for (var i_1 = 0; i_1 <= 5; i_1++) {
    _loop_1(i_1);
}
//# sourceMappingURL=letinfor.js.map