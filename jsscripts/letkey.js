//let keyword characteristics
function letkey(num) {
    var ab;
    //let key word cannot be used anywhere inside the function
    //, it should only be declared at the top not after variable declaration
    if (num == 6) {
        ab = 6;
    }
    else {
        ab = 7;
    }
    console.log(ab);
}
letkey(5);
var a = 1;
var b = 2;
if (a == 1) {
    var a = 10;
    var b_1 = 20;
    console.log(a);
    console.log(b_1);
    //let scope lies inside the block
}
console.log(a);
console.log(b);
//let b=2; (shows an error) //let cannot be declared multiple times unlike var key word.
//# sourceMappingURL=letkey.js.map