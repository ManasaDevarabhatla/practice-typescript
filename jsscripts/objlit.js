var _a;
var firstname = "Joey";
var lastname = "Tribbiani";
var age = 20;
var person = {
    firstname: firstname,
    lastname: lastname,
    age: age,
    isSenior: function () {
        return age > 50;
    }
};
console.log(person.firstname, person.lastname, person.age, person.isSenior());
var ln = "lastname";
var person2 = (_a = {},
    _a[ln] = "Geller",
    _a["first"] = "Ross",
    _a);
console.log(person2["first"], person2[ln]);
//# sourceMappingURL=objlit.js.map