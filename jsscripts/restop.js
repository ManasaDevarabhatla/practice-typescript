var __spreadArrays = (this && this.__spreadArrays) || function () {
    for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
    for (var r = Array(s), k = 0, i = 0; i < il; i++)
        for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
            r[k] = a[j];
    return r;
};
var displaycolors = function (message) {
    var colors = [];
    for (var _i = 1; _i < arguments.length; _i++) {
        colors[_i - 1] = arguments[_i];
    }
    console.log(message);
    //here colors is an array
    console.log(colors);
    for (var i_1 in colors) {
        console.log(colors[i_1]);
    }
};
//if the rest op is not defined, this message is going to appear for every
//iteration above and for every declaration below
//otherwise it only appears once before every list.
var message = "list of colors";
displaycolors(message, 'red');
displaycolors(message, 'red', 'blue');
//spread operator is opposite to rest, it declared during function call but not function declaration
var colorsarray = ['green', 'yellow', 'pink'];
displaycolors.apply(void 0, __spreadArrays([message], colorsarray));
//# sourceMappingURL=restop.js.map