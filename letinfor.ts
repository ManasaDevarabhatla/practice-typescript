for(var i=0;i<=5;i++){
    console.log(i);
}
/** when we give var keyword it is refering to the end value of i but not the 
 * value at each iteration
 * 
 * for(var i=0;i<=5;i++){
    setTimeout(function(){console.log(i);},1000);
}*/

// therefore while using closures and loops let keyword is used.
for(let i=0;i<=5;i++){
    setTimeout(function(){console.log(i);},1000);
}
