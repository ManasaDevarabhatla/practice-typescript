var emp ={
    id:1,
    im:2,
    greet: function(){
        var self = this;
        setTimeout(() => {console.log(self.im)},1000);
        //same statement above can be written in two ways
        setTimeout(function()
        {
            console.log(self.id)},1000);
    }
};
emp.greet();

//the output is shown undefined because "this" keyword here refers
//to the function inside but not the outside function context.

let getvalue3 = function(value = 10, bonus=5){
    console.log(value,bonus);
    console.log(arguments.length);
};
getvalue3(); //takes default value
getvalue3(20);
getvalue3(20,30);
getvalue3(undefined,30);