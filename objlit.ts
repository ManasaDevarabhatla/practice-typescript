let firstname = "Joey";
let lastname =  "Tribbiani";
let age = 20;
let person = {
    firstname,
    lastname,
    age,
    isSenior: function(){
        return age>50;
    }
};
console.log(person.firstname, person.lastname,person.age,person.isSenior());

let ln = "lastname"
let person2={
    [ln]:"Geller",
    "first":"Ross"
};
console.log(person2["first"],person2[ln]);