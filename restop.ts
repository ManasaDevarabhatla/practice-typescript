let displaycolors = function(message, ...colors){ //here the three dots represent rest operator
    console.log(message);
    //here colors is an array
    console.log(colors);
    for(let i in colors){
        console.log(colors[i]);
    }
}
//if the rest op is not defined, this message is going to appear for every
//iteration above and for every declaration below
//otherwise it only appears once before every list.
let message = "list of colors";
displaycolors(message,'red');
displaycolors(message,'red','blue');

//spread operator is opposite to rest, it declared during function call but not function declaration
let colorsarray = ['green','yellow','pink'];
displaycolors(message, ...colorsarray);
